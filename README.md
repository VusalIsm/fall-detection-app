# Fall Detection App

The app provides an falling information based on the accelaration from CoreMotion library. After It detect that the device has fallen, It starts an count down. If the user press correspoding button during the count down, the app turns to the main page. But the owner fails to push the button, the app automatically calls the provided number. For my case, it is ``194``, but it can be any number.

## Screen Shots
<img src="https://drive.google.com/uc?export=view&id=1Tiw36IcRz3w-4gynCiDRg4Yip7pMH4iV" alt="drawing" width="200"/>
<img src="https://drive.google.com/uc?export=view&id=1owQTrNOjjx8RzsicM4s-7o0uIgUgAy5n" alt="drawing" width="200"/>
<img src="https://drive.google.com/uc?export=view&id=1vbF9mhKHgD3LABjRo_LjfYL0iKU_VqEj" alt="drawing" width="200"/>

### Developer: Vusal Ismayilov (vusalism99@gmail.com)
