//
//  ViewController.swift
//  Fall Detection
//
//  Created by Vusal Ismayilov on 2021. 07. 03..
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var stopCountdownButton: UIButton!
    var timer = Timer()
    var seconds = 10
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Fall Detection View Initiated")
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.countdown), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func stopCountdown(_ sender: Any) {
        timer.invalidate()
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func countdown(){
        seconds -= 1
        timerLabel.text = String(seconds)
        
        if(seconds == 0){
            timer.invalidate()
            print("Timer Finished")
            callNumber(phoneNumber: "194")
        }
    }
    
    private func callNumber(phoneNumber:String) {
        guard let number = URL(string: "tel://" + phoneNumber) else { return }
        if UIApplication.shared.canOpenURL(number) {
            UIApplication.shared.open(number)
        } else {
            print("Can't open url on this device")
        }
        
    }
}

