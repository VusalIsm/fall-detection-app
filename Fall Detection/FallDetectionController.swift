//
//  FallDetectionController.swift
//  Fall Detection
//
//  Created by Vusal Ismayilov on 2021. 07. 03..
//

import Foundation
import UIKit
import CoreMotion


class FallDetectionController: UIViewController {
    let manager = CMMotionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Main storyboard initiated")
        manager.startAccelerometerUpdates()
        manager.startGyroUpdates()
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true){ _ in
            if let data = self.manager.accelerometerData {
                if  (abs(data.acceleration.x) + abs(data.acceleration.y) + abs(data.acceleration.z)) >= 6.25{
                    print("falled")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "fallDetection") as! ViewController
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "fallDetection") as! ViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
